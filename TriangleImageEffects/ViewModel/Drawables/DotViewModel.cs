﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleImageEffects.ViewModel
{
    class DotViewModel : DrawableViewModel, Model.Interfaces.IDot
    {
        readonly Model.Interfaces.IDot _dot;
        public double Radius { get; }

        public DotViewModel(Model.Interfaces.IDot dot, double viewRadius)
            : base(dot)
        {
            if (dot == null)
                throw new ArgumentNullException(nameof(dot));

            _dot = dot;
            Radius = viewRadius;
        }
    }
}
