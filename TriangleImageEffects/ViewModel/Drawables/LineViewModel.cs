﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.ViewModel
{
    class LineViewModel : DrawableViewModel, ILine
    {
        ILine _line;
        public double Length => _line.Length;

        public double LengthSquared => _line.LengthSquared;

        public double X1
        {
            get { return _line.X1; }
            set
            {
                if (_line.X1 == value) return;
                _line.X1 = value;
                OnPropertyChanged(nameof(X1));
            }
        }

        public double X2
        {
            get { return _line.X2; }
            set
            {
                if (_line.X2 == value) return;
                _line.X2 = value;
                OnPropertyChanged(nameof(X2));
            }
        }

        public double Y1
        {
            get { return _line.Y1; }
            set
            {
                if (_line.Y1 == value) return;
                _line.Y1 = value;
                OnPropertyChanged(nameof(Y1));
            }
        }

        public double Y2
        {
            get { return _line.Y2; }
            set
            {
                if (_line.Y2 == value) return;
                _line.Y2 = value;
                OnPropertyChanged(nameof(Y2));
            }
        }
        public Point StartPoint => new Point(X1, Y1);
        public Point EndPoint => new Point(X2, Y2);
        public LineViewModel(ILine line) 
            : base(line)
        {
            if (line == null)
                throw new ArgumentNullException(nameof(line));

            _line = line;
        }
    }
}
