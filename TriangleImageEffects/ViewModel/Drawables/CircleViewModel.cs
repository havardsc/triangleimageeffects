﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleImageEffects.ViewModel
{
    class CircleViewModel : EllipseViewModel, Model.Interfaces.ICircle
    {
        readonly Model.Interfaces.ICircle _circle;

        public double Radius => _circle.Radius;

        public CircleViewModel(Model.Interfaces.ICircle circle)
            : base(circle)
        {
            if (circle == null)
                throw new ArgumentNullException(nameof(circle));

            _circle = circle;
        }
    }
}
