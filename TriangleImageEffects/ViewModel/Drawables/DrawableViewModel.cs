﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using TriangleImageEffects.Model;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.ViewModel
{
    abstract class DrawableViewModel : ViewModelBase, IDrawable
    {
        readonly IDrawable _drawable;

        double _viewScale;
        public double ViewScale
        {
            get { return _viewScale; }
            set
            {
                if (_viewScale == value) return;
                _viewScale = value;
                OnPropertyChanged(nameof(ViewScale));
            }
        }
        public Point Center
        {
            get { return _drawable.Center; }
            set
            {
                if (_drawable.Center.X == value.X && _drawable.Center.Y == value.Y) return;
                _drawable.Center = value;
                OnPropertyChanged(nameof(Center));
            }
        }
        public double X
        {
            get { return _drawable.X; }
            set
            {
                if (_drawable.X == value) return;
                _drawable.X = value;
                OnPropertyChanged(nameof(X));
            }
        }

        public double Y
        {
            get { return _drawable.Y; }
            set
            {
                if (_drawable.Y == value) return;
                _drawable.Y = value;
                OnPropertyChanged(nameof(Y));
            }
        }

        public double Height => _drawable.Height;
        public double Width => _drawable.Width;

        public Color Color
        {
            get { return _drawable.Color; }
            set
            {
                if (_drawable.Color == value)
                    return;
                _drawable.Color = value;
                OnPropertyChanged(nameof(Color));
            }
        }

        private double _scale;

        public double Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                OnPropertyChanged(nameof(Scale));
            }
        }

        bool _isVisible = true;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (_isVisible == value) return;
                _isVisible = value;
                OnPropertyChanged(nameof(IsVisible));
            }
        }
        public IBoundingBox BoundingBox => _drawable.BoundingBox;

        public DrawableViewModel(IDrawable drawable)
        {
            if (drawable == null)
                throw new ArgumentNullException(nameof(drawable));

            _drawable = drawable;
        }
    }
}
