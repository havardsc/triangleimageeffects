﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace TriangleImageEffects.ViewModel
{
    class EllipseViewModel : DrawableViewModel, Model.Interfaces.IEllipse
    {
        readonly Model.Interfaces.IEllipse _ellipse;

        public double RadiusX => _ellipse.RadiusX;
        public double RadiusY => _ellipse.RadiusY;

        public EllipseViewModel(Model.Interfaces.IEllipse ellipse)
            : base(ellipse)
        {
            if (ellipse == null)
                throw new ArgumentNullException(nameof(ellipse));

            _ellipse = ellipse;
        }
    }
}
