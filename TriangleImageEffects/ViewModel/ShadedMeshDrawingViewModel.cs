﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TriangleImageEffects.Model.ImageProcessing;
using TriangleNet.Geometry;

namespace TriangleImageEffects.ViewModel
{
    class ShadedMeshDrawingViewModel : DrawingViewModel
    {
        ProcessBitmapBase _imageProcessor;
        public double MeshScale { get; set; }
        public ProcessBitmapBase ImageProcessor
        {
            get { return _imageProcessor; }
            set
            {
                if (_imageProcessor == value) return;
                _imageProcessor = value;
                OnPropertyChanged(nameof(ImageProcessor));
            }
        }
        WriteableBitmap _shadingImage;
        public WriteableBitmap ShadingImage
        {
            get { return _shadingImage; }
            private set
            {
                if (_shadingImage == value) return;
                _shadingImage = value;
                OnPropertyChanged(nameof(ShadingImage));
            }
        }

        TriangleNet.Mesh _mesh;
        public TriangleNet.Mesh Mesh
        {
            get { return _mesh; }
            set
            {
                if (_mesh == value) return;
                _mesh = value;
                OnPropertyChanged(nameof(Mesh));
            }
        }
        bool _isGeneratingImage;
        public bool IsGeneratingImage
        {
            get { return _isGeneratingImage; }
            set
            {
                if (_isGeneratingImage == value) return;
                _isGeneratingImage = value;
                OnPropertyChanged(nameof(IsGeneratingImage));
            }
        }

        ICommand _shadeCommand;
        public ICommand ShadeCommand
        {
            get
            {
                if (_shadeCommand == null)
                    _shadeCommand = new RelayCommand<System.Windows.Controls.Control>(Shade, i => _mesh != null && _imageProcessor != null);
                return _shadeCommand;
            }
        }

        public ShadedMeshDrawingViewModel()
        {
            PropertyChanged += ShadedMeshDrawingViewModel_PropertyChanged;
        }

        void Shade(System.Windows.Controls.Control control)
        {
            if (_imageProcessor == null)
                return;
            WriteableBitmap writeableBitmap = null;

            var width = control.ActualWidth;
            var height = control.ActualHeight;
            var transform = Transform.Value;


            writeableBitmap = new WriteableBitmap((int)width, (int)height, 96, 96, PixelFormats.Bgr32, null);

            var sw = new Stopwatch();

            sw.Start();
            _imageProcessor.ProcessBitmap(writeableBitmap, new Int32Rect(0, 0, writeableBitmap.PixelWidth, writeableBitmap.PixelHeight), transform);
            sw.Stop();

            Console.WriteLine($"{height} x {width}: {sw.ElapsedMilliseconds}");


            ShadingImage = writeableBitmap;
        }

        void ShadedMeshDrawingViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Mesh):
                    UpdateGeometry();
                    break;
                case nameof(ViewWidth):
                case nameof(ViewHeight):
                    CenterTransform();
                    ShadingImage = null;
                    break;
                case nameof(UniformScale):
                    ShadingImage = null;
                    break;
            }
        }



        void UpdateGeometry()
        {
            var sw = new Stopwatch();
            sw.Start();
            Drawables.Clear();
            //foreach (var point in Mesh.Vertices)
            //    Drawables.Add(CreateDot(100, point.X, point.Y, Colors.Red, localScale));
            //Drawables.Add(CreateCircle(5, point.X, point.Y, Colors.Red, localScale));

            var visitedSegments = new HashSet<System.Windows.Point>();

            foreach (var segment in Mesh.Segments)
            {
                Drawables.Add(CreateLine(segment));
                var v0 = segment.GetVertex(0);
                var v1 = segment.GetVertex(1);
                var tCenter = new System.Windows.Point((v0.X + v1.X) * 0.5, (v0.Y + v1.Y) * 0.5);
                visitedSegments.Add(tCenter);
            }


            foreach (var triangle in Mesh.Triangles)
                for (int i = 0; i < 3; i++)
                {
                    var v0 = triangle.GetVertex(i);
                    var v1 = triangle.GetVertex((i + 1) % 3);
                    var tCenter = new System.Windows.Point((v0.X + v1.X) * 0.5, (v0.Y + v1.Y) * 0.5);

                    if (visitedSegments.Add(tCenter))
                        Drawables.Add(CreateLine(v0.X, v0.Y, v1.X, v1.Y));
                }

            var bb = BoundingBox;
            Drawables.Add(CreateLine(bb.Min.X, bb.Min.Y, bb.Max.X, bb.Min.Y));
            Drawables.Add(CreateLine(bb.Max.X, bb.Min.Y, bb.Max.X, bb.Max.Y));
            Drawables.Add(CreateLine(bb.Max.X, bb.Max.Y, bb.Min.X, bb.Max.Y));
            Drawables.Add(CreateLine(bb.Min.X, bb.Max.Y, bb.Min.X, bb.Min.Y));

            sw.Stop();
            Console.WriteLine($"Creating {Drawables.Count} drawables: {sw.ElapsedMilliseconds} ms");
        }

        static LineViewModel CreateLine(ISegment segment, double scale = 1, System.Windows.Point offset = default(System.Windows.Point))
        {
            var p0 = segment.GetVertex(0);
            var p1 = segment.GetVertex(1);

            return CreateLine(p0.X, p0.Y, p1.X, p1.Y, scale, offset);
        }
        static LineViewModel CreateLine(double x1, double y1, double x2, double y2, double scale = 1, System.Windows.Point offset = default(System.Windows.Point))
        {
            return new LineViewModel(
                new Model.Line(
                    (x1 - offset.X) * scale,
                    (y1 - offset.Y) * scale,
                    (x2 - offset.X) * scale,
                    (y2 - offset.Y) * scale)
                {
                    Color = Colors.Green
                });
        }
        static CircleViewModel CreateCircle(double radius, double x, double y, Color color, double scale = 1, System.Windows.Point offset = default(System.Windows.Point))
        {
            return new CircleViewModel(new Model.Circle(radius) { X = (x - offset.X) * scale, Y = (y - offset.Y) * scale, Color = color });
        }

        static DotViewModel CreateDot(double radius, double x, double y, Color color, double scale = 1, System.Windows.Point offset = default(System.Windows.Point))
        {
            return new DotViewModel(new Model.Dot((x - offset.X) * scale, (y - offset.X) * scale) { Color = color }, radius);
        }
    }
}
