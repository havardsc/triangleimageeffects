﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using TriangleImageEffects.Model;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.ViewModel
{
    class DrawingViewModel : ViewModelBase
    {
        public ObservableCollection<DrawableViewModel> Drawables { get; } = new ObservableCollection<DrawableViewModel>();

        bool _isBoundingBoxDirty = true;
        IBoundingBox _boundingBox;
        public IBoundingBox BoundingBox
        {
            get
            {
                if (_isBoundingBoxDirty)
                {
                    _boundingBox = new BoundingBox(Drawables);
                    _isBoundingBoxDirty = false;
                }
                return _boundingBox;
            }
        }
        double _uniformScale;
        public double UniformScale
        {
            get { return _uniformScale; }
            set
            {
                if (_uniformScale == value) return;
                _uniformScale = value;
                var centerX = Transform.Value.OffsetX / Transform.Value.M11;
                var centerY = Transform.Value.OffsetY / Transform.Value.M22;

                //this.Transform.Value.ScaleAt(value, value, centerX, centerY);
                var trGroup = Transform as TransformGroup;
                var scaleTr = trGroup.Children[1] as ScaleTransform;
                scaleTr.ScaleX = scaleTr.ScaleY = _uniformScale;
                
                OnPropertyChanged(nameof(UniformScale));
                OnPropertyChanged(nameof(Transform));
            }
        }

        Transform _transform = Transform.Identity;
        public Transform Transform
        {
            get { return _transform; }
            protected set
            {
                if (_transform == value) return;
                _transform = value;
                OnPropertyChanged(nameof(Transform));
            }
        }

        bool _isVisible = true;
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (_isVisible == value) return;
                _isVisible = value;
                OnPropertyChanged(nameof(IsVisible));
            }
        }

        double _viewHeight;
        public double ViewHeight
        {
            get { return _viewHeight; }
            set
            {
                if (_viewHeight == value) return;
                _viewHeight = value;
                OnPropertyChanged(nameof(ViewHeight));
            }
        }
        double _viewWidth;
        public double ViewWidth
        {
            get { return _viewWidth; }
            set
            {
                if (_viewWidth == value) return;
                _viewWidth = value;
                OnPropertyChanged(nameof(ViewWidth));
            }
        }
        public DrawingViewModel()
        {
            Transform = Transform.Identity;
            PropertyChanged += DrawingViewModel_PropertyChanged;
            Drawables.CollectionChanged += Drawables_CollectionChanged;
        }

        private void Drawables_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            _isBoundingBoxDirty = true;
        }

        public void CenterTransform()
        {
            var sw = new Stopwatch();
            sw.Start();

            var bb = BoundingBox;
            var transformGroup = new TransformGroup();

            var maxBB = Math.Max(bb.Width, bb.Height);
            var minView = Math.Min(ViewWidth, ViewHeight);
            var scaleAmount = minView / maxBB;

            transformGroup.Children.Add(new TranslateTransform(-bb.Center.X, -bb.Center.Y));
            transformGroup.Children.Add(new ScaleTransform(scaleAmount, scaleAmount));
            Transform = transformGroup;
            UniformScale = scaleAmount;

            sw.Stop();
            Console.WriteLine($"Center view: {sw.ElapsedMilliseconds} ms");
        }

        private void DrawingViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Transform):
                    SetDrawableRenderScale();
                    break;
            }
        }

        void SetDrawableRenderScale()
        {
            foreach (var drawable in Drawables)
                drawable.ViewScale = Transform.Value.M11;
        }
    }
}
