﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriangleImageEffects.Model;
using TriangleImageEffects.Model.ImageProcessing;
using TriangleImageEffects.ViewModel;
using TriangleNet;
using TriangleNet.Geometry;

namespace TriangleImageEffects
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            //var bounds = new TriangleNet.Geometry.Rectangle(-1.0, -1.0, 2.0, 2.0);
            //var mesh2 = TriangleNet.Meshing.GenericMesher.StructuredMesh(bounds, 1, 1);// as Mesh;
            //var mesh = mesh2 as Mesh;
            var polygon = TriangleNet.IO.FileProcessor.Read(@"D:\Downloads\Triangle.NET\Triangle.NET\Data\superior.poly");
            var mesh = polygon.Triangulate(
                new TriangleNet.Meshing.ConstraintOptions() { ConformingDelaunay = true },
                new TriangleNet.Meshing.QualityOptions() { MaximumArea = 0.0001 }) as Mesh;

            var rnd = new Random();
            var vertexWeightsDict = new Dictionary<int, double>();
            foreach (var id in mesh.Vertices.Select(i => i.ID))
                vertexWeightsDict.Add(id, rnd.NextDouble());
            
            var polygonFillProcessor = new PolygonFillProcessor(mesh, vertexWeightsDict);
            var simpleProcessor = new SimpleProcessBitmap();

            var shadedDrawingVm = this.Drawing.DataContext as ShadedMeshDrawingViewModel;
            shadedDrawingVm.Mesh = mesh;
            shadedDrawingVm.ImageProcessor = polygonFillProcessor;
            //shadedDrawingVm.ImageProcessor = simpleProcessor;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
