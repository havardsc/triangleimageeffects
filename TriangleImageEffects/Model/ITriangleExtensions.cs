﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleNet.Geometry;

namespace TriangleImageEffects.Model
{
    static class ITriangleExtensions
    {
        public static BarycentricCoord GetBarycentricCoordFromPoint(this ITriangle triangle, double x, double y)
        {
            var v1 = triangle.GetVertex(0);
            var v2 = triangle.GetVertex(1);
            var v3 = triangle.GetVertex(2);

            return new BarycentricCoord(x, y, v1.X, v1.Y, v2.X, v2.Y, v3.X, v3.Y);
        }
    }
}
