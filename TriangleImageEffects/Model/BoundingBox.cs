﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.Model
{
    struct BoundingBox : IBoundingBox
    {
        public Point Center { get; }
        public double Height { get; }
        public Point Max { get; }
        public Point Min { get; }
        public double Width { get; }

        public BoundingBox(IEnumerable<IDrawable> drawables)
        {
            double minX = double.MaxValue;
            double minY = double.MaxValue;
            double maxX = double.MinValue;
            double maxY = double.MinValue;

            foreach (var drawable in drawables)
            {
                var bb = drawable.BoundingBox;
                if (bb.Min.X < minX)
                    minX = bb.Min.X;
                if (bb.Max.X > maxX)
                    maxX = bb.Max.X;

                if (bb.Min.Y < minY)
                    minY = bb.Min.Y;
                if (bb.Max.Y > maxY)
                    maxY = bb.Max.Y;
            }

            Min = new Point(minX, minY);
            Max = new Point(maxX, maxY);
            Width = maxX - minX;
            Height = maxY - minY;
            Center = new Point((minX + maxX) * 0.5, (minY + maxY) * 0.5);
        }

        public BoundingBox(Point min, Point max)
        {
            Min = min;
            Max = max;
            Height = max.X - min.X;
            Width = max.Y - min.Y;
            Center = new Point((min.X + max.X) * 0.5, (min.Y + max.Y) * 0.5);

        }

        public override string ToString() => $"Min:{Min}\tMax:{Max}";
    }
}
