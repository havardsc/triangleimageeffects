﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleImageEffects.Model
{
    struct BarycentricCoord
    {
        public double W1 { get; }
        public double W2 { get; }
        public double W3 { get; }
        public BarycentricCoord(double x, double y, double x1, double y1, double x2, double y2, double x3, double y3, double determinant = default(double))
        {
            if (determinant == default(double))
                determinant = (y2 - y3) * (x1 - x3) + (x3 - x2) * (y1 - y3);

            W1 = ((y2 - y3) * (x - x3) + (x3 - x2) * (y - y3)) / determinant;
            W2 = ((y3 - y1) * (x - x3) + (x1 - x3) * (y - y3)) / determinant;
            W3 = 1 - W1 - W2;
        }
    }
}
