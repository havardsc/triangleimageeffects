﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.Model
{
    class Ellipse : Drawable, Interfaces.IEllipse
    {
        public double RadiusX { get; }
        public double RadiusY { get; }

        IBoundingBox _boundingBox;
        public override IBoundingBox BoundingBox
        {
            get
            {
                if (_boundingBox == null)
                {
                    var size = new Vector(RadiusX, RadiusY);
                    _boundingBox = new BoundingBox(Point.Subtract(Center, size), Point.Add(Center, size));
                }
                return _boundingBox;
            }
        }

        public Ellipse(double radiusX, double radiusY)
            : base(radiusX * 2, radiusY * 2)
        {
            RadiusX = radiusX;
            RadiusY = radiusY;
        }
    }
}
