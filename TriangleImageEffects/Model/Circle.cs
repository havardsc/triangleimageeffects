﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleImageEffects.Model
{
    class Circle : Ellipse, Interfaces.ICircle
    {
        public double Radius { get; }
        public Circle(double radius) 
            : base(radius, radius)
        {
            Radius = radius;
        }
    }
}
