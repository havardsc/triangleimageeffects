﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.Model
{
    class Line : Drawable, ILine
    {
        public override double X
        {
            get { return (X1 + X2) * 0.5; }
            set
            {
                var curX = X;
                if (curX == value) return;
                var diff = value - curX;
                X1 += diff;
                X2 += diff;
                Width = X2 - X1;
            }
        }
        public override double Y
        {
            get { return (Y1 + Y2) * 0.5; }
            set
            {
                var curY = Y;
                if (curY == value) return;
                var diff = value - curY;
                Y1 += diff;
                Y2 += diff;
                Height = Y2 - Y1;
            }
        }
        public double LengthSquared => Math.Pow(X2 - X1, 2) + Math.Pow(Y2 - Y1, 2);
        public double Length => Math.Sqrt(LengthSquared);
        double _x1;
        public double X1
        {
            get { return _x1; }
            set { _x1 = value; _isBoundingBoxDirty = true; }
        }
        double _y1;
        public double Y1
        {
            get { return _y1; }
            set { _y1 = value; _isBoundingBoxDirty = true; }
        }

        double _x2;
        public double X2
        {
            get { return _x2; }
            set { _x2 = value; _isBoundingBoxDirty = true; }
        }
        double _y2;
        public double Y2
        {
            get { return _y2; }
            set { _y2 = value; _isBoundingBoxDirty = true; }
        }

        bool _isBoundingBoxDirty = true;
        IBoundingBox _boundingBox;
        public override IBoundingBox BoundingBox
        {
            get
            {
                if (_isBoundingBoxDirty)
                {
                    double minX = X1, minY = Y1, maxX = X2, maxY = Y2;
                    if (X1 > X2)
                    {
                        minX = X2;
                        maxX = X1;
                    }

                    if (Y1 > Y2)
                    {
                        minY = Y2;
                        maxY = Y1;
                    }
                    _boundingBox = new BoundingBox(new Point(minX, minY), new Point(maxX, maxY));
                    _isBoundingBoxDirty = false;
                }
                return _boundingBox;
            }

        }

        public Line(Point startPoint, Point endPoint)
            : this(startPoint.X, startPoint.Y, endPoint.X, endPoint.Y)
        {

        }
        public Line(double x1, double y1, double x2, double y2)
            : base(y2 - y1, x2 - x1)
        {
            X1 = x1; Y1 = y1;
            X2 = x2; Y2 = y2;
        }
    }
}
