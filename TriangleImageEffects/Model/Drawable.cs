﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.Model
{
    abstract class Drawable : IDrawable
    {
        public Point Center
        {
            get { return new Point(X, Y); }
            set
            {
                if (X == value.X && Y == value.Y) return;
                X = value.X;
                Y = value.Y;
            }
        }
        public virtual double X { get; set; }
        public virtual double Y { get; set; }
        public virtual double Width { get; set; }
        public virtual double Height { get; set; }
        public Color Color { get; set; }
        public abstract IBoundingBox BoundingBox { get; }

        public Drawable (double height, double width)
        {
            Height = height;
            Width = width;
        }
    }
}
