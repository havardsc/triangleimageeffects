﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TriangleNet;
using TriangleNet.Meshing;

namespace TriangleImageEffects.Model.ImageProcessing
{
    class PolygonFillProcessor : ProcessBitmapBase
    {
        readonly TriangleNet.Tools.TriangleQuadTree _quadTree;
        readonly Mesh _mesh;
        readonly IReadOnlyDictionary<int, double> _vertexWeights;
        protected override int BytesPerPixel => 4;

        double _startX, _startY, _scaleX, _scaleY;
        public PolygonFillProcessor(Mesh mesh, IReadOnlyDictionary<int, double> vertexWeights)
        {
            if (mesh == null)
                throw new ArgumentNullException(nameof(mesh));
            if (vertexWeights == null)
                throw new ArgumentNullException(nameof(vertexWeights));

            _vertexWeights = vertexWeights;
            _mesh = mesh;
            _quadTree = new TriangleNet.Tools.TriangleQuadTree(mesh);
        }

        protected override byte[] CreateColorData(int x, int y)
        {
            var xm = _startX + x / _scaleX;
            var ym = _startY + y / _scaleY;

            var triangle = _quadTree.Query(xm, ym);
            if (triangle != null)
            {
                var bary = triangle.GetBarycentricCoordFromPoint(xm, ym);

                var v1Weight = _vertexWeights[triangle.GetVertexID(0)];
                var v2Weight = _vertexWeights[triangle.GetVertexID(1)];
                var v3Weight = _vertexWeights[triangle.GetVertexID(2)];

                v1Weight *= bary.W1;
                v2Weight *= bary.W2;
                v3Weight *= bary.W3;

                var weightSum = (v1Weight + v2Weight + v3Weight) / 1.0;

                return new byte[] { (byte)(255 * weightSum), (byte)(255 * (1 - weightSum)), 0 };
                //return new byte[] { (byte)(255 * bary.W1), (byte)(255 * bary.W2), (byte)(255 * bary.W3) };
            }

            return new byte[] { 255, 255, 255 };
        }

        protected override void Preprocess(Int32Rect dirtyRect, System.Windows.Media.Matrix transform)
        {
            _scaleX = transform.M11;
            _scaleY = transform.M22;

            var centerX = -transform.OffsetX / _scaleX;
            var centerY = -transform.OffsetY / _scaleY;

            _startX = centerX - dirtyRect.Width * 0.5  / _scaleX;
            _startY = centerY - dirtyRect.Height *0.5 / _scaleY;
        }
    }
}
