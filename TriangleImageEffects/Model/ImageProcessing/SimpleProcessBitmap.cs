﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace TriangleImageEffects.Model.ImageProcessing
{
    class SimpleProcessBitmap : ProcessBitmapBase
    {
        protected override int BytesPerPixel => 4;
        double _width, _height;
        protected override byte[] CreateColorData(int x, int y)
        {
            var b = x * 255 / _width;
            var g = y * 255 / _height;
            var r = 0;
            //System.Diagnostics.Debug.Assert(b < 255);
            //System.Diagnostics.Debug.Assert(g < 255);
            return new byte[] { (byte)b, (byte)g, (byte)r/*, 128 */};
        }

        protected override void Preprocess(Int32Rect dirtyRect, Matrix transform)
        {
            _height = dirtyRect.Height;
            _width = dirtyRect.Width;
        }
    }
}

