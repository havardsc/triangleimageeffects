﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace TriangleImageEffects.Model.ImageProcessing
{
    abstract class ProcessBitmapBase
    {
        protected abstract int BytesPerPixel { get; }
        protected abstract byte[] CreateColorData(int x, int y);
        protected abstract void Preprocess(Int32Rect dirtyRect, System.Windows.Media.Matrix transform);
        public void ProcessBitmap(
            WriteableBitmap writeableBitmap, Int32Rect dirtyRect, System.Windows.Media.Matrix transform)//System.Windows.Media.Transform transform)
        {
            if (writeableBitmap == null)
                throw new ArgumentNullException(nameof(writeableBitmap));
            if (dirtyRect == null)
                throw new ArgumentNullException(nameof(dirtyRect));
            if (transform == null)
                throw new ArgumentNullException(nameof(transform));

            if (dirtyRect.Y < 0 || dirtyRect.Y + dirtyRect.Height > writeableBitmap.PixelHeight ||
                dirtyRect.X < 0 || dirtyRect.X + dirtyRect.Width > writeableBitmap.PixelWidth)
                throw new ArgumentOutOfRangeException(nameof(dirtyRect));
            
            writeableBitmap.Lock();
            unsafe
            {
                byte* backBuffer = (byte*)writeableBitmap.BackBuffer;
                int backBufferStride = writeableBitmap.BackBufferStride;
                int width = writeableBitmap.PixelWidth;
                int height = writeableBitmap.PixelHeight;

                Preprocess(dirtyRect, transform);

                Parallel.For(0, height, y =>
                {
                    byte* currentLine = backBuffer + (y * backBufferStride);
                    for (int x = 0; x < backBufferStride; x += BytesPerPixel)
                    {
                        byte[] colorData = CreateColorData(x / BytesPerPixel, y);

                        for (int i = 0; i < colorData.Length; i++)
                            currentLine[x + i] = colorData[i];
                    }
                });
            }
            writeableBitmap.AddDirtyRect(dirtyRect);
            writeableBitmap.Unlock();
        }
    }
}
