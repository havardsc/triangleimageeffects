﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleImageEffects.Model.Interfaces
{
    interface IBoundingBox
    {
        System.Windows.Point Center { get; }
        System.Windows.Point Min { get; }
        System.Windows.Point Max { get; }
        double Width { get; }
        double Height { get; }
    }
}
