﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace TriangleImageEffects.Model.Interfaces
{
    interface IDrawable 
    {
        IBoundingBox BoundingBox { get; }
        Point Center { get; set; }
        double X { get; set; }
        double Y { get; set; }
        double Width { get; }
        double Height { get; }
        Color Color { get; set; }
    }
}
