﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleImageEffects.Model.Interfaces
{
    interface ICircle : IEllipse
    {
        double Radius { get; }
    }
}
