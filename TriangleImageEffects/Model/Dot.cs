﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleImageEffects.Model.Interfaces;

namespace TriangleImageEffects.Model
{
    class Dot : Drawable, IDot
    {
        public override double X
        {
            get { return base.X; }
            set
            {
                base.X = value;
                _isBoundingBoxDirty = true;
            }
        }

        public override double Y
        {
            get { return base.Y; }
            set
            {
                base.Y = value;
                _isBoundingBoxDirty = true;
            }
        }
        public Dot(System.Windows.Point position)
            : base(0, 0)
        {
            Center = position;
        }

        public Dot(double x, double y)
            : this(new System.Windows.Point(x, y))
        {
        }

        bool _isBoundingBoxDirty = true;
        IBoundingBox _boundingBox;
        public override IBoundingBox BoundingBox
        {
            get
            {
                if (_isBoundingBoxDirty)
                {
                    _boundingBox = new BoundingBox(Center, Center);
                    _isBoundingBoxDirty = false;
                }
                return _boundingBox;
            }
        }
    }
}
